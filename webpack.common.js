var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: './src/global/js/app.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    publicPath: '/',
    filename: 'bundle.js',
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel-loader',
      options: {
        presets: ['@babel/preset-env', '@babel/preset-react'],
        plugins: ['@babel/plugin-transform-runtime'],
      },
    }],
  },
  resolve: {
    extensions: ['*', '.js', '.json'],
    alias: {
      'config': path.resolve(__dirname, './config-dist'),
    },
  },
  plugins: [
    new webpack.ProvidePlugin({
      React: 'react',
      ReactDOM: 'react-dom',
      Rhaetia: 'rhaetia',
      A: ['rhaetia', 'A'],
      Config: 'config',
    }),
  ],
}
