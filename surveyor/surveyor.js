const fs = require('fs');
const http = require('http');
const ProgressBar = require('progress');

(async function() {
  const path = './lyrics';
  const artists = fs.readdirSync(path);
  const etymologies = [];
  for (let i=0; i<artists.length; i++) {
    if (artists[i] === '.DS_Store') { continue }
    let artist = {
      name: artists[i].substring(5),
      albums: [],
    };
    const albums = fs.readdirSync(`${path}/${artists[i]}`);
    for (let j=0; j<albums.length; j++) {
      if (albums[j] === '.DS_Store') { continue }
      if (albums[j] === 'meta.json') {
        const meta = JSON.parse(fs.readFileSync(`${path}/${artists[i]}/meta.json`), 'utf-8');
        artist = {...artist, ...meta};
        continue;
      }
      let album = {
        name: albums[j].substring(5),
        songs: [],
      };
      const songs = fs.readdirSync(`${path}/${artists[i]}/${albums[j]}`);
      const bar = new ProgressBar(`:percent :etas :bar ${artists[i].substring(5)} - ${albums[j].substring(5)}`, {
        width: 30,
        incomplete: ' ',
        total: songs.length,
      });
      for (let k=0; k<songs.length; k++) {
        if (songs[k] === '.DS_Store') { continue }
        if (songs[k] === 'meta.json') {
          const meta = JSON.parse(fs.readFileSync(`${path}/${artists[i]}/${albums[j]}/meta.json`), 'utf-8');
          album = {...album, ...meta};
          continue;
        }
        const title = songs[k].split('.txt')[0].substring(5);
        const lyrics = fs.readFileSync(`${path}/${artists[i]}/${albums[j]}/${songs[k]}`, 'utf8');

        const response = await call({text: lyrics});
        bar.tick(1);

        if (!response.result) {
          console.error(`ERR! ${title}`);
          console.error(response)
          continue;
        }

        let percentages = {
          de: 0,
          fr: 0,
          no: 0,
          la: 0,
          xx: 0,
        };
        const words = Object.keys(response.result.words);
        words.forEach(word => percentages[getCategory(response.result.words[word].source)]++);
        const sum = words.length;
        percentages.de = percentages.de / sum * 100;
        percentages.fr = percentages.fr / sum * 100;
        percentages.no = percentages.no / sum * 100;
        percentages.la = percentages.la / sum * 100;
        percentages.xx = percentages.xx / sum * 100;

        album.songs.push({
          title: title,
          etymology: percentages,
        });
      }
      artist.albums.push(album);
    }
    etymologies.push(artist);
  }
  fs.writeFileSync('./results.json', JSON.stringify(etymologies));
})();

function getCategory(ety) {
  switch (ety) {
    case 'de': // german
    case 'ang': // old english
    case 'gml': // middle low german
    case 'gem-pro': // proto-germanic
    case 'gmw-pro': // proto-west-germanic
    case 'gmq-pro': // proto-north-germanic
    case 'gmq': // north-germanic
      return 'de';
    case 'fr': // french
    case 'fro': // old french
    case 'ONF.': // old northern french
    case 'xno': // anglo-norman
    case 'frm': // middle french
      return 'fr';
    case 'non': // old norse
      return 'no';
    case 'la': // latin
    case 'ML.': // medieval latin
    case 'OL.': // old latin
    case 'LL.': // late latin
      return 'la';
    default:
      return 'xx';
  }
}

function call(data) {
  return new Promise((resolve, reject) => {
    const payload = JSON.stringify(data);
    const req = http.request({
      host: '127.0.0.1',
      port: '7001',
      path: '/api/v1/etymologies',
      method: 'post',
      headers: {
        'Content-Type': 'application/json;charset=UTF-8',
        'Content-Length': Buffer.byteLength(payload),
      },
    }, (res) => {
      let responseString = '';
      res.on('data', (data) => {
          responseString += data;
      });
      res.on('end', () => {
        try {
          const data = JSON.parse(responseString);
          resolve(data);
        }
        catch(e) {
          resolve({
            err: responseString,
            result: null,
          });
        }
      });
    });
    req.write(payload);
    req.end();
  });
}
