<?php

  namespace Mieda;

  $router = new Router();

  $router->post(   'etymologies', 'Etymology::get');

  $router->get(    'ignored-words', 'IgnoredWord::get');

?>
