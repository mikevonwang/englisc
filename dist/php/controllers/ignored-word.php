<?php

  namespace Mieda;

  class IgnoredWord extends Controller {

    public static function get() {
      if (self::c()) {
        $ignore_list = json_decode(file_get_contents(__DIR__ . '/../../db/ignore_list.json', FILE_USE_INCLUDE_PATH));
        self::$result = [
          'ignoredWords' => $ignore_list,
        ];
      }
      return self::output();
    }

  }

?>
