<?php

  namespace Mieda;

  class Etymology extends Controller {

    public static function get() {
      if (self::c()) {
        global $config;
        if (strlen(self::$data['text']) === 0) {
          self::$err = 'text_empty';
          http_response_code(400);
        }
        else if (strlen(self::$data['text']) > $config['max_length']) {
          self::$err = 'text_too_long';
          http_response_code(400);
        }
      }
      if (self::c()) {
        $dictionary = file_get_contents(__DIR__ . '/../../db/dictionary.json', FILE_USE_INCLUDE_PATH);
        $ignore_list = json_decode(file_get_contents(__DIR__ . '/../../db/ignore_list.json', FILE_USE_INCLUDE_PATH));

        $words_raw = self::$data['text'];
        $words_raw = self::ascii_transliterate($words_raw);
        $words_raw = strtolower($words_raw);
        $words_raw = preg_replace('/[\.\,\?\!\(\)\"]/', '', $words_raw);
        $words_raw = preg_replace('/\-/', ' ', $words_raw);

        $word_list = preg_split('/\s/', $words_raw);
        $word_list = array_filter($word_list, function($var) {
          return (strlen($var) > 0);
        });

        $words = [];
        foreach ($word_list as $word_raw) {
          $word = trim($word_raw);
          $word = preg_replace('/n\'$/', 'ng', $word);
          $word = preg_replace('/\'s$/', '', $word);
          $word = preg_replace('/^\'?(cause|coz|cuz|cos|cus)\'?$/', 'because', $word);
          $word = preg_replace('/^(gonna|gunna|imma)$/', 'going', $word);
          $word = preg_replace('/^\'?(til|till)$/', 'until', $word);
          if (in_array($word, $ignore_list) == false) {
            if (array_key_exists($word, $words)) {
              $words[$word]['count'] += 1;
            }
            else {
              $words[$word] = [
                'count' => 1,
              ];
            }
          }
        }

        foreach (array_keys($words) as $word) {
          $words[$word]['source'] = self::getEtymology($dictionary, $word);
        }

        self::$result = [
          'words' => $words,
        ];
      }
      self::recordHit(strlen(self::$data['text']), (is_null(self::$err) && !is_null(self::$result)));
      return self::output();
    }

    private static function recordHit($length, $success) {
      $now = date('Y-m-d H-i-s');
      self::query([
        'INSERT INTO hits',
        '(datetime, address, length, success) VALUES (',
          '"' . $now . '",',
          '"' . $_SERVER['REMOTE_ADDR'] . '",',
          $length . ',',
          $success .
        ')',
      ]);
    }

    private static function getEtymology($dictionary, $word, $count = 5) {
      if ($count == 0) {
        return null;
      }
      else {
        $entry_start   = strpos($dictionary, '"' . $word . '":{');
        if ($entry_start == false) { return null; }

        $snippet_start = strpos($dictionary, '":{', $entry_start);
        $snippet_end   = strpos($dictionary, '"}',  $entry_start);
        if ($snippet_start == false || $snippet_end == false) { return null; }

        $snippet = substr($dictionary, $snippet_start + 2, $snippet_end - $snippet_start);
        $entry = json_decode($snippet);
        if (is_null($entry)) { return null; }

        if (property_exists($entry, 'ety')) {
          return $entry->ety;
        }
        else if (property_exists($entry, 'lemma')) {
          return self::getEtymology($dictionary, $entry->lemma, $count - 1);
        }
        else {
          return null;
        }
      }
    }

  }

?>
