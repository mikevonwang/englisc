<?php

  namespace Mieda;

  global $jwt_key;

  abstract class Controller {

    protected static $err;
    protected static $result;
    protected static $temp;
    protected static $qry;
    protected static $ans;
    protected static $data;
    protected static $con;
    protected static $env;
    protected static $const;

    public static function init($data, $con, $env, $const) {
      self::$con = $con;
      self::$err = NULL;
      self::$result = NULL;
      self::$temp = [];
      self::$qry = [];
      self::$ans = NULL;
      self::$data = $data;
      self::$env = $env;
      self::$const = $const;
    }

    private static function sanitize($data) {
      return array_map(function($value) {
        if (is_array($value)) {
          return self::sanitize($value);
        }
        else if (is_object($value)) {
          return self::sanitize((array) $value);
        }
        else if (is_null($value)) {
          return null;
        }
        else {
          return mysqli_real_escape_string(self::$con, $value);
        }
      }, $data);
    }

    protected static function c() {
      if (is_null(self::$err)) {
        self::$ans = NULL;
        return true;
      }
      else {
        return false;
      }
    }

    protected static function output() {
      if (!(self::c())) {
        self::$result = NULL;
      }
      $output = [
        'err' => self::$err,
        'result' => self::$result,
        'qry' => self::$qry
      ];
      return $output;
    }

    protected static function query($qry_raw) {
      $qry = join(' ', $qry_raw);
      self::$qry[] = $qry;
      self::$ans = mysqli_query(self::$con, $qry);
      if (!self::$ans) {
        if (self::$env === 'local') {
          self::$err = mysqli_error(self::$con);
        }
        else {
          self::$err = 'server_error';
        }
        http_response_code(500);
      }
      return self::$ans;
    }

    public static function check_token() {
      if (self::c()) {
        if (!isset(self::$data['token'])) {
          self::$err = 'invalid_token';
          http_response_code(401);
        }
      }
      if (self::c()) {
        try {
          global $config;
          $decoded = \JWT::decode(self::$data['token'], $config['jwt_key']);
          self::$temp = array(
            'token_user_id' => $decoded->user_id,
            'token_username' => $decoded->username,
          );
        }
        catch (\Exception $e) {
          self::$err = 'invalid_token';
          http_response_code(401);
        }
      }
      if (self::c()) {
        self::query([
          'UPDATE users',
          'SET date_activity = NOW()',
          'WHERE user_ID = ' . self::$temp['token_user_id'],
        ]);
      }
    }

    public static function log($output) {
      file_put_contents('php://stdout', "\n" . print_r($output,true) . "\n");
    }

    protected static function get_external_url($url, $timeout = 5) {
      $curl = curl_init();
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_TIMEOUT, $timeout);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      $html = curl_exec($curl);
      if ($html) {
        return $html;
      }
      else {
        return false;
      }
    }

    protected static function check_id_validity($id) {
      return (!preg_match('/\D/', $id));
    }

    public static function any_value($table, $columns) {
      return join(', ', array_map(function($column) use ($table) {
        return ('ANY_VALUE(' . $table . '.' . $column . ') AS ' . $column);
      }, $columns));
    }

    public static function send_mail($recipient, $subject, $body) {
      try {
        global $config;

        $url = '';
        switch ($_SERVER['SERVER_NAME']) {
          case $config['env']['local']['server_name']:
            $url = $config['env']['local']['app_url'];
          break;
          case $config['env']['staging']['server_name']:
            $url = $config['env']['staging']['app_url'];
          break;
          case $config['env']['live']['server_name']:
            $url = $config['env']['live']['app_url'];
          break;
        }

        $mail = new \PHPMailer\PHPMailer\PHPMailer(true);

        $mail->isSMTP();
        $mail->Host =       $config['email']['host'];
        $mail->SMTPSecure = $config['email']['security'];
        $mail->Port =       $config['email']['port'];
        $mail->SMTPAuth =   true;
        $mail->Username =   $config['email']['username'];
        $mail->Password =   $config['email']['password'];

        $mail->setFrom($config['email']['from']['email'], $config['email']['from']['name']);
        $mail->addAddress($recipient);

        $mail->isHTML(true);
        $mail->Subject = $subject;

        $mail->Body = (new Email($recipient, $subject, $body, $url))->get_html();

        $mail->Send();
      }
      catch (Exception $e) {
        self::$err = 'mailer_failed';
        http_response_code(500);
      }
    }

    public static function base64_url_encode($input) {
      return strtr(base64_encode($input), '+/=', '._-');
    }

    public static function base64_url_decode($input) {
      return base64_decode(strtr($input, '._-', '+/='));
    }

    public static function ascii_transliterate($s) {
      $replace = array(
        'ъ'=>'-', 'Ь'=>'-', 'Ъ'=>'-', 'ь'=>'-',
        'Ă'=>'A', 'Ą'=>'A', 'À'=>'A', 'Ã'=>'A', 'Á'=>'A', 'Æ'=>'A', 'Â'=>'A', 'Å'=>'A', 'Ä'=>'Ae',
        'Þ'=>'B',
        'Ć'=>'C', 'ץ'=>'C', 'Ç'=>'C',
        'È'=>'E', 'Ę'=>'E', 'É'=>'E', 'Ë'=>'E', 'Ê'=>'E',
        'Ğ'=>'G',
        'İ'=>'I', 'Ï'=>'I', 'Î'=>'I', 'Í'=>'I', 'Ì'=>'I',
        'Ł'=>'L',
        'Ñ'=>'N', 'Ń'=>'N',
        'Ø'=>'O', 'Ó'=>'O', 'Ò'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'Oe',
        'Ş'=>'S', 'Ś'=>'S', 'Ș'=>'S', 'Š'=>'S',
        'Ț'=>'T',
        'Ù'=>'U', 'Û'=>'U', 'Ú'=>'U', 'Ü'=>'Ue',
        'Ý'=>'Y',
        'Ź'=>'Z', 'Ž'=>'Z', 'Ż'=>'Z',
        'â'=>'a', 'ǎ'=>'a', 'ą'=>'a', 'á'=>'a', 'ă'=>'a', 'ã'=>'a', 'Ǎ'=>'a', 'а'=>'a', 'А'=>'a', 'å'=>'a', 'à'=>'a', 'א'=>'a', 'Ǻ'=>'a', 'Ā'=>'a', 'ǻ'=>'a', 'ā'=>'a', 'ä'=>'ae', 'æ'=>'ae', 'Ǽ'=>'ae', 'ǽ'=>'ae',
        'б'=>'b', 'ב'=>'b', 'Б'=>'b', 'þ'=>'b',
        'ĉ'=>'c', 'Ĉ'=>'c', 'Ċ'=>'c', 'ć'=>'c', 'ç'=>'c', 'ц'=>'c', 'צ'=>'c', 'ċ'=>'c', 'Ц'=>'c', 'Č'=>'c', 'č'=>'c', 'Ч'=>'ch', 'ч'=>'ch',
        'ד'=>'d', 'ď'=>'d', 'Đ'=>'d', 'Ď'=>'d', 'đ'=>'d', 'д'=>'d', 'Д'=>'D', 'ð'=>'d',
        'є'=>'e', 'ע'=>'e', 'е'=>'e', 'Е'=>'e', 'Ə'=>'e', 'ę'=>'e', 'ĕ'=>'e', 'ē'=>'e', 'Ē'=>'e', 'Ė'=>'e', 'ė'=>'e', 'ě'=>'e', 'Ě'=>'e', 'Є'=>'e', 'Ĕ'=>'e', 'ê'=>'e', 'ə'=>'e', 'è'=>'e', 'ë'=>'e', 'é'=>'e',
        'ф'=>'f', 'ƒ'=>'f', 'Ф'=>'f',
        'ġ'=>'g', 'Ģ'=>'g', 'Ġ'=>'g', 'Ĝ'=>'g', 'Г'=>'g', 'г'=>'g', 'ĝ'=>'g', 'ğ'=>'g', 'ג'=>'g', 'Ґ'=>'g', 'ґ'=>'g', 'ģ'=>'g',
        'ח'=>'h', 'ħ'=>'h', 'Х'=>'h', 'Ħ'=>'h', 'Ĥ'=>'h', 'ĥ'=>'h', 'х'=>'h', 'ה'=>'h',
        'î'=>'i', 'ï'=>'i', 'í'=>'i', 'ì'=>'i', 'į'=>'i', 'ĭ'=>'i', 'ı'=>'i', 'Ĭ'=>'i', 'И'=>'i', 'ĩ'=>'i', 'ǐ'=>'i', 'Ĩ'=>'i', 'Ǐ'=>'i', 'и'=>'i', 'Į'=>'i', 'י'=>'i', 'Ї'=>'i', 'Ī'=>'i', 'І'=>'i', 'ї'=>'i', 'і'=>'i', 'ī'=>'i', 'ĳ'=>'ij', 'Ĳ'=>'ij',
        'й'=>'j', 'Й'=>'j', 'Ĵ'=>'j', 'ĵ'=>'j', 'я'=>'ja', 'Я'=>'ja', 'Э'=>'je', 'э'=>'je', 'ё'=>'jo', 'Ё'=>'jo', 'ю'=>'ju', 'Ю'=>'ju',
        'ĸ'=>'k', 'כ'=>'k', 'Ķ'=>'k', 'К'=>'k', 'к'=>'k', 'ķ'=>'k', 'ך'=>'k',
        'Ŀ'=>'l', 'ŀ'=>'l', 'Л'=>'l', 'ł'=>'l', 'ļ'=>'l', 'ĺ'=>'l', 'Ĺ'=>'l', 'Ļ'=>'l', 'л'=>'l', 'Ľ'=>'l', 'ľ'=>'l', 'ל'=>'l',
        'מ'=>'m', 'М'=>'m', 'ם'=>'m', 'м'=>'m',
        'ñ'=>'n', 'н'=>'n', 'Ņ'=>'n', 'ן'=>'n', 'ŋ'=>'n', 'נ'=>'n', 'Н'=>'n', 'ń'=>'n', 'Ŋ'=>'n', 'ņ'=>'n', 'ŉ'=>'n', 'Ň'=>'n', 'ň'=>'n',
        'о'=>'o', 'О'=>'o', 'ő'=>'o', 'õ'=>'o', 'ô'=>'o', 'Ő'=>'o', 'ŏ'=>'o', 'Ŏ'=>'o', 'Ō'=>'o', 'ō'=>'o', 'ø'=>'o', 'ǿ'=>'o', 'ǒ'=>'o', 'ò'=>'o', 'Ǿ'=>'o', 'Ǒ'=>'o', 'ơ'=>'o', 'ó'=>'o', 'Ơ'=>'o', 'œ'=>'oe', 'Œ'=>'oe', 'ö'=>'oe',
        'פ'=>'p', 'ף'=>'p', 'п'=>'p', 'П'=>'p',
        'ק'=>'q',
        'ŕ'=>'r', 'ř'=>'r', 'Ř'=>'r', 'ŗ'=>'r', 'Ŗ'=>'r', 'ר'=>'r', 'Ŕ'=>'r', 'Р'=>'r', 'р'=>'r',
        'ș'=>'s', 'с'=>'s', 'Ŝ'=>'s', 'š'=>'s', 'ś'=>'s', 'ס'=>'s', 'ş'=>'s', 'С'=>'s', 'ŝ'=>'s', 'Щ'=>'sch', 'щ'=>'sch', 'ш'=>'sh', 'Ш'=>'sh', 'ß'=>'ss',
        'т'=>'t', 'ט'=>'t', 'ŧ'=>'t', 'ת'=>'t', 'ť'=>'t', 'ţ'=>'t', 'Ţ'=>'t', 'Т'=>'t', 'ț'=>'t', 'Ŧ'=>'t', 'Ť'=>'t', '™'=>'tm',
        'ū'=>'u', 'у'=>'u', 'Ũ'=>'u', 'ũ'=>'u', 'Ư'=>'u', 'ư'=>'u', 'Ū'=>'u', 'Ǔ'=>'u', 'ų'=>'u', 'Ų'=>'u', 'ŭ'=>'u', 'Ŭ'=>'u', 'Ů'=>'u', 'ů'=>'u', 'ű'=>'u', 'Ű'=>'u', 'Ǖ'=>'u', 'ǔ'=>'u', 'Ǜ'=>'u', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'У'=>'u', 'ǚ'=>'u', 'ǜ'=>'u', 'Ǚ'=>'u', 'Ǘ'=>'u', 'ǖ'=>'u', 'ǘ'=>'u', 'ü'=>'ue',
        'в'=>'v', 'ו'=>'v', 'В'=>'v',
        'ש'=>'w', 'ŵ'=>'w', 'Ŵ'=>'w',
        'ы'=>'y', 'ŷ'=>'y', 'ý'=>'y', 'ÿ'=>'y', 'Ÿ'=>'y', 'Ŷ'=>'y',
        'Ы'=>'y', 'ž'=>'z', 'З'=>'z', 'з'=>'z', 'ź'=>'z', 'ז'=>'z', 'ż'=>'z', 'ſ'=>'z', 'Ж'=>'zh', 'ж'=>'zh'
      );
      return strtr($s, $replace);
    }

  }

?>
