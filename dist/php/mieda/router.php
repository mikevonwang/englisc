<?php

  namespace Mieda;

  class Router {

    public $routes = [];
    public $params = [];

    public function get($route, $handler, $protected = false) {
      $this->routes[] = [
        'method' => 'GET',
        'path' => explode('/', $route),
        'handler' => $handler,
        'protected' => $protected,
      ];
    }

    public function post($route, $handler, $protected = false) {
      $this->routes[] = [
        'method' => 'POST',
        'path' => explode('/', $route),
        'handler' => $handler,
        'protected' => $protected,
      ];
    }

    public function put($route, $handler, $protected = false) {
      $this->routes[] = [
        'method' => 'PUT',
        'path' => explode('/', $route),
        'handler' => $handler,
        'protected' => $protected,
      ];
    }

    public function delete($route, $handler, $protected = false) {
      $this->routes[] = [
        'method' => 'DELETE',
        'path' => explode('/', $route),
        'handler' => $handler,
        'protected' => $protected,
      ];
    }

    public function match($method, $uri) {
      $this->params = [];
      $handler = null;
      $protected = null;
      foreach($this->routes as $route) {
        if ($method == $route['method'] && count($route['path']) == count($uri)) {
          foreach($route['path'] as $i=>$piece) {
            $match = true;
            if (substr($piece,0,1) == ':') {
              $this->params[substr($piece,1)] = $uri[$i];
            }
            else if ($piece != $uri[$i]) {
              $match = false;
              break;
            }
          }
          if ($match == true) {
            $handler = $route['handler'];
            $protected = $route['protected'];
            break;
          }
        }
      }
      return [
        'handler' => $handler,
        'params' => array_slice($this->params, 0),
        'protected' => $protected,
      ];
    }

  }

?>
