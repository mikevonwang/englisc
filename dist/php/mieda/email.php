<?php

  namespace Mieda;

  class Email {

    private $html;

    public function __construct($recipient, $subject, $body, $url) {
      ob_start();
      ?>

      <html>
        <head>
          <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <title><?php echo $subject; ?></title>
          <style type="text/css">
            #outlook a {padding:0;}
            body {width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}
            .ExternalClass {width:100%;}
            .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
            #backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}
            img {outline:none; border: none; padding: 0; text-decoration:none; -ms-interpolation-mode: bicubic; }
            a img {border:none;}
            .image_fix {display:block;}
            table td {border-collapse: collapse; vertical-align: middle;}
            table { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;}
          </style>
        </head>
        <body style="width: 100%; margin: 0; padding: 0; background-color: #ffffff;">
          <table width="600" align="center" style="border-collapse: collapse; font-family: Arial; background-color: #eeeeee;">

            <tr>
              <td width="2%"></td>
              <td width="4%"></td>
              <td width="42%"></td>
              <td width="4%"></td>
              <td width="42%"></td>
              <td width="4%"></td>
              <td width="2%"></td>
            </tr>

            <tr height="30px"><td colspan="5"></td></tr>
            <tr>
              <td></td>
              <td colspan="5" style="text-align: center">
                <img src="https://compass.study/img/email/wordmark.png" width="300px" height="75px" alt="Compass"/>
              </td>
              <td></td>
            </tr>
            <tr height="30px"><td colspan="5"></td></tr>
            <tr height="40px"><td></td><td colspan="5" style="background-color: #ffffff"></td><td></td></tr>

            <tr>
              <td></td>
              <td style="background-color: #ffffff"></td>
              <td colspan="3" style="background-color: #ffffff">
                <?php echo str_replace('’', '&#39;', join(' ', $body)); ?>
              </td>
              <td style="background-color: #ffffff"></td>
              <td></td>
            </tr>
            <tr height="30px"><td></td><td colspan="5" style="background-color: #ffffff"></td><td></td></tr>

            <tr height="30px"><td colspan="5"></td></tr>
            <tr>
              <td></td>
              <td colspan="5" style="text-align: center">
                <img src="https://compass.study/img/email/logo.png" width="60px" height="60px" alt="Compass logo"/>
              </td>
              <td></td>
            </tr>

            <tr height="30px"><td colspan="5"></td></tr>
            <tr>
              <td colspan="2"></td>
              <td colspan="3" style="text-align: center">
                <p>This email was sent to <b><a href="mailto:<?php echo $recipient; ?>" style="color: #86419f; text-decoration: none"><?php echo $recipient; ?></a></b>.</p>
              </td>
              <td colspan="2"></td>
            </tr>

            <tr height="30px"><td colspan="5"></td></tr>

          </table>
        </body>
      </html>

      <?php
      $this->html = ob_get_contents();
      ob_end_clean();
    }

    public function get_html() {
      return $this->html;
    }

  }

  ob_start();
?>
