<?php

  namespace Mieda;

  class Constant {

    private $items;

    public function __construct($items) {
      $this->items = $items;
    }

    public function get_label($value) {
      if ($this->items[$value]) {
        return $this->items[$value];
      }
      else {
        return null;
      }
    }

    public function get_value($label) {
      return array_search($label, $this->items);
    }

  }

?>
