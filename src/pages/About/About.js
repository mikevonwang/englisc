export default function About(props) {

  return (
    <main className="aux about">
      <h2>About Englisc</h2>
      <p><strong>Englisc</strong> is an etymology tool. Given some Modern English text, it calculates the relative percentages of unique words in that text that descend from <a href="https://en.wikipedia.org/wiki/Old_English">Old English</a> and <a href="https://en.wikipedia.org/wiki/Old_French">Old French</a> (and a few other sources).</p>
      <p>The name “Englisc” is the Old English word for “English” itself. Old English encountered significant <a href="https://en.wikipedia.org/wiki/Norman_conquest_of_England">influence</a> from foreign languages like Old French and Old Norse during the 11th and 12th centuries, after which the language developed into Middle English.</p>
      <p>Words that descend from these foreign tongues (especially those from Old French) often coexist synonymously with the descendants of native Old English words, but carry different connotations. <strong>Englisc</strong> serves to examine a particular author’s choices in vocabulary.</p>
      <p>This tool was built by <a href="https://mikevonwang.com">Mike Wang</a> in the autumn of 2020, after wondering whether the native language of the Icelandic band <a href="https://en.wikipedia.org/wiki/Of_Monsters_and_Men">Of Monsters and Men</a> influenced the <a href="https://mikevonwang.com/blog/does-the-native-language-of-a-songwriter-influence-the-etymology-of-their-songs-in-english">lyrics</a> of their songs.</p>
      <p><A href="/">‹ Back</A></p>
    </main>
  );

}
