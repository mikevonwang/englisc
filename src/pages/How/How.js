export default function How(props) {

  return (
    <main className="aux how">
      <h2>How Englisc works</h2>
      <p><strong>Englisc</strong> uses a database dump from Wiktionary as its source of etymologic data, and the <a href="https://github.com/mikevonwang/engsource">Engsource</a> tool to manipulate that data. Engsource extracts the most recent origin (besides Middle English) of every entry from the English Wiktionary, and compiles those origins into a list. When you press “GO”, Englisc splits your source text into words, then checks that list for the origin of every word.</p>
      <p>Many grammatical words, like “this” or “you”, tend to have no synonyms in Modern English. <A href="/ignored">These words</A> are ignored.</p>
      <p><A href="/">‹ Back</A></p>
    </main>
  );

}
