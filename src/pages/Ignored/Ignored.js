import { xhr } from '../../global/js/xhr';

export default function Ignored(props) {

  const [ignoredWords, setIgnoredWords] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(true);
  const [error, setError] = React.useState(null);

  React.useEffect(() => {
    getIgnored();
  }, []);

  async function getIgnored() {
    try {
      const response = await xhr('get', '/ignored-words');
      setIgnoredWords(response.ignoredWords);
    }
    catch(err) {
      setError(true);
    }
    setIsLoading(false);
  }

  return (
    <main className="aux ignored">
      <h2>Ignored words</h2>
      <p><strong>Englisc</strong> ignores certain words in any text that it’s given. These words serve grammatical purposes in English, and aren’t subject to the same etymologic forces as nouns or verbs. The list of all ignored words is as follows:</p>
      {ignoredWords ? (
        <ul className="word-list">
          {
            ignoredWords.map((word, i) => {
              return (
                <li key={i}>{word}</li>
              );
            })
          }
        </ul>
      ) : (
        <>
          {isLoading && <p>Loading...</p>}
          {error && (
            <p className="error">
              {error === true && 'Oops! Something went wrong.'}
            </p>
          )}
        </>
      )}
      <p><A href="/">‹ Back</A></p>
    </main>
  );

}
