import scale from 'scale-color-perceptual';

import { xhr } from '../../global/js/xhr';

import Spinner from '../../modules/Spinner/Spinner';
import GraphBar from '../../modules/GraphBar/GraphBar';
import LegendEntry from '../../modules/LegendEntry/LegendEntry';

const TEXT_EMPTY = 'text_empty';
const TEXT_TOO_LONG = 'text_too_long';

export default function Main(props) {

  const [data, setData] = React.useState(null);
  const [lang, setLang] = React.useState('de');
  const [hover, setHover] = React.useState(null);
  const [isLoading, setIsLoading] = React.useState(false);
  const [error, setError] = React.useState(null);

  const textarea = React.useRef();

  React.useEffect(() => {
    if (location.hostname === Config.env.live.location_hostname) {
      console.log = () => {};
    }
  }, []);

  async function onClick() {
    if (isLoading === false) {
      setError(null);
      setIsLoading(true);
      setData(null);
      const text = textarea.current.value;
      if (text.length === 0) {
        setError(TEXT_EMPTY);
      }
      else if (text.length > Config.max_length) {
        setError(TEXT_TOO_LONG);
      }
      else {
        try {
          const { words } = await xhr('post', '/etymologies', {text});
          let counts = {
            fr: 0,
            de: 0,
            no: 0,
            la: 0,
            xx: 0,
          }
          Object.keys(words).forEach((word) => {
            counts[getCategory(words[word].source)]++;
          });
          const total = Object.keys(words).length;
          setData({
            words,
            counts: {
              fr: counts.fr / total * 100,
              de: counts.de / total * 100,
              no: counts.no / total * 100,
              la: counts.la / total * 100,
              xx: counts.xx / total * 100,
            },
          });
        }
        catch(err) {
          console.error(err);
          if (err === TEXT_EMPTY || err === TEXT_TOO_LONG) {
            setError(err);
          }
          else {
            setError(true);
          }
        }
      }
      setIsLoading(false);
    }
  }

  function getColorForIndex(index) {
    return scale.magma((index+0.5)/7);
  }

  function getCategory(ety) {
    switch (ety) {
      case 'fr': // french
      case 'fro': // old french
      case 'ONF.': // old northern french
      case 'xno': // anglo-norman
      case 'frm': // middle french
        return 'fr';
      case 'de': // german
      case 'ang': // old english
      case 'gml': // middle low german
      case 'gem-pro': // proto-germanic
      case 'gmw-pro': // proto-west-germanic
      case 'gmq-pro': // proto-north-germanic
      case 'gmq': // north-germanic
        return 'de';
      case 'non': // old norse
        return 'no';
      case 'la': // latin
      case 'ML.': // medieval latin
      case 'OL.': // old latin
      case 'LL.': // late latin
        return 'la';
      default:
        return 'xx';
    }
  }

  return (
    <main className="main">
      <section>
        <h2>Source Text</h2>
        <textarea className="chalk" ref={textarea} placeholder="Paste some text and click “GO”"/>
        <cite>Built with data from Wiktionary</cite>
      </section>
      <section>
        <button className="btn blue" onClick={onClick} disabled={isLoading}>GO</button>
        <Spinner is_active={isLoading} size="mini"/>
      </section>
      <section>
        <h2>Etymologic Analysis</h2>
        {data ? (
          <>
            <div className="graph">
              <GraphBar code="de" data={data} color={getColorForIndex(5)} onClick={setLang} setHover={setHover} hover={hover}/>
              <GraphBar code="fr" data={data} color={getColorForIndex(4)} onClick={setLang} setHover={setHover} hover={hover}/>
              <GraphBar code="no" data={data} color={getColorForIndex(3)} onClick={setLang} setHover={setHover} hover={hover}/>
              <GraphBar code="la" data={data} color={getColorForIndex(2)} onClick={setLang} setHover={setHover} hover={hover}/>
              <GraphBar code="xx" data={data} color={getColorForIndex(1)} onClick={setLang} setHover={setHover} hover={hover}/>
            </div>
            <div className="graph-scale">
              <p style={{left: '0%'}}>0%</p>
              <p style={{left: '10%'}}/>
              <p style={{left: '20%'}}>20%</p>
              <p style={{left: '30%'}}/>
              <p style={{left: '40%'}}>40%</p>
              <p style={{left: '50%'}}/>
              <p style={{left: '60%'}}>60%</p>
              <p style={{left: '70%'}}/>
              <p style={{left: '80%'}}>80%</p>
              <p style={{left: '90%'}}/>
              <p style={{left: '100%'}}>100%</p>
            </div>
            <div className="graph-aux">
              <div className="graph-aux-part aux-part-legend">
                <h3>Legend</h3>
                <div className="legend">
                  <LegendEntry code="de" label="Old English" lang={lang} data={data} color={getColorForIndex(5)} onClick={setLang} setHover={setHover} hover={hover}/>
                  <LegendEntry code="fr" label="Old French"  lang={lang} data={data} color={getColorForIndex(4)} onClick={setLang} setHover={setHover} hover={hover}/>
                  <LegendEntry code="no" label="Old Norse"   lang={lang} data={data} color={getColorForIndex(3)} onClick={setLang} setHover={setHover} hover={hover}/>
                  <LegendEntry code="la" label="Latin"       lang={lang} data={data} color={getColorForIndex(2)} onClick={setLang} setHover={setHover} hover={hover}/>
                  <LegendEntry code="xx" label="Other"       lang={lang} data={data} color={getColorForIndex(1)} onClick={setLang} setHover={setHover} hover={hover}/>
                </div>
              </div>
              <div className="graph-aux-part aux-part-word-list">
                <h3>Word List</h3>
                <ul className="word-list">
                  {Object.keys(data.words).filter(key => getCategory(data.words[key].source) === lang).sort().map(word => <li key={word}>{word}</li>)}
                </ul>
              </div>
            </div>
          </>
        ) : (
          <>
            {isLoading && <p>Loading...</p>}
            {error && (
              <p className="error">
                {error === true && 'Oops! Something went wrong.'}
                {error === TEXT_EMPTY && 'Text cannot be empty.'}
                {error === TEXT_TOO_LONG && `Text cannot be more than ${Config.max_length} characters.`}
              </p>
            )}
            {!isLoading && !error && <p>Waiting for user input.</p>}
          </>
        )}
      </section>
    </main>
  );

}
