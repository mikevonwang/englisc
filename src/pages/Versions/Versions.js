export default function Versions(props) {

  return (
    <main className="aux verisons">
      <h2>Versions</h2>
      <ul>
        <li>
          <h3>v1.0.0</h3>
          <p>engsource@<strong>1.0.0</strong></p>
          <p>enwiktionary from <strong>2020-09-20</strong></p>
        </li>
      </ul>
      <p><A href="/">‹ Back</A></p>
    </main>
  );

}
