export default function GraphBar(props) {

  return (
    <div
      id={props.code}
      className={`graph-bar ${props.code} ${props.hover === props.code ? 'hover' : ''}`}
      style={{width: `${props.data.counts[props.code]}%`, backgroundColor: props.color}}
      onClick={() => props.onClick(props.code)}
      onMouseOver={() => props.setHover(props.code)}
      onMouseOut={() => props.setHover(null)}
    />
  );

}
