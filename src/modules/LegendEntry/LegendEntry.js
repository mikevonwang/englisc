export default function LegendEntry(props) {

  return (
    <div className={`legend-entry ${props.lang === props.code ? 'current' : ''} ${props.hover === props.code ? 'hover' : ''}`} onClick={() => props.onClick(props.code)} onMouseOver={() => props.setHover(props.code)} onMouseOut={() => props.setHover(null)}>
      <div className="color" style={{backgroundColor: props.color}}/>
      <label htmlFor={props.code}>{props.label}</label>
      <span>{`${parseInt(props.data.counts[props.code])}%`}</span>
    </div>
  );

}
