export function xhr(method, path, body = {}, timeout = 10000) {
  return new Promise((resolve, reject) => {
    if (body === null) {
      body = {};
    }
    console.log('%c' + '\n\n\n>> ' + method.toUpperCase() + ' ' + path, 'color: #1483c0');
    console.log(body);
    let url;
    switch (location.hostname) {
      case Config.env.local.location_hostname:
        url = Config.env.local.api_url + path;
      break;
      case Config.env.live.location_hostname:
        url = Config.env.live.api_url + path;
      break;
    }
    if (method === 'get') {
      const keys = Object.keys(body);
      if (keys.length > 0) {
        url += '?' + keys.map((key) => {
          return (encodeURIComponent(key) + '=' + encodeURIComponent(body[key]));
        }).join('&');
      }
    }
    const xhr = new XMLHttpRequest();
    xhr.onload = () => {
      console.log('%c' + xhr.responseText, 'color: #444');
      var res;
      try {
        res = JSON.parse(xhr.responseText);
        if (res.err === null) {
          resolve(res.result);
        }
        else {
          reject(res.err);
        }
      }
      catch (e) {
        reject(xhr.responseText);
      }
    };
    xhr.onerror = () => {
      reject('connection_error');
    };
    xhr.ontimeout = () => {
      reject('timeout_error');
    };
    xhr.open(method, url, true);
    xhr.timeout = timeout;
    xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
    if (localStorage.getItem('token') !== null) {
      xhr.setRequestHeader('Authorization', 'Bearer ' + localStorage.getItem('token'));
    }
    if (method === 'get') {
      xhr.send();
    }
    else {
      xhr.send(JSON.stringify(body));
    }
  });
};
