import Main from '../../pages/Main/Main';
import About from '../../pages/About/About';
import How from '../../pages/How/How';
import Versions from '../../pages/Versions/Versions';
import Ignored from '../../pages/Ignored/Ignored';

function App(props) {

  return (
    <>
      <header>
        <A href="/" className="logo">
          <img src="/img/logo.png"/>
          <h1>Englisc</h1>
        </A>
        <A href="/about">What is this?</A>
        <A href="/how">How it works</A>
        <A className="version-label" href="/versions">v1.0.0</A>
      </header>
      {Rhaetia.renderChild(props.children)}
    </>
  );

};

const route_tree = [
  ['', Main],
  ['about', About],
  ['how', How],
  ['versions', Versions],
  ['ignored', Ignored],
];

ReactDOM.render((
  <Rhaetia.Router routeTree={route_tree} fallbackURL={'/'}>
    <App/>
  </Rhaetia.Router>
), document.getElementById('roots'));
